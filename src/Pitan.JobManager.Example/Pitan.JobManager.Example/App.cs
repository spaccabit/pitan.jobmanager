﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pitan.JobManager.Extensions;
using Pitan.JobManager.Example.Jobs;

using Xamarin.Forms;
using Pitan.JobManager.Device;

namespace Pitan.JobManager.Example
{
    public class App : Application
    {
        private IJobManager jobManager;
        private IDevice device;

        public App(IJobManager jobManager, IDevice device)
        {
            this.jobManager = jobManager;
            this.device = device;

            var addJobButton = new Button
            {
                HorizontalOptions = LayoutOptions.Center,
                Text = "Add a download job",


            };

            addJobButton.Clicked += AddJobButton_Clicked;

            // The root page of your application
            MainPage = new ContentPage
            {
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        new Label {
                            HorizontalTextAlignment = TextAlignment.Center,
                            Text = "Welcome to Pitan JobManager Examples."
                        },
                        
                        addJobButton
                    }
                }
            };
        }

        private void AddJobButton_Clicked(object sender, EventArgs e)
        {
            var parameter = JobParameter.Builder().IsWakeLock(false).Retry().WithRequrements(;
            var testJob = new DownloadJob(parameter, this.device);
            
            this.jobManager.Add(testJob);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
