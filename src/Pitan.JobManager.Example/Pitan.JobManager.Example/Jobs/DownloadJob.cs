﻿using System;
using Pitan.JobManager.Device;
using System.Net.Http;
using System.Diagnostics;

namespace Pitan.JobManager.Example.Jobs
{
    public class DownloadJob : Job
    {
        public DownloadJob(JobParameter jobParameter, IDevice device) : base(jobParameter, device)
        {


        }

        public override void Added()
        {
            Debug.WriteLine("Work added");
        }

        public override void Canceled()
        {
            Debug.WriteLine("Work canseld");
        }

        public override void Run()
        {
            using (var client = new HttpClient())
            {
                var response = client.GetAsync("https://dl.dropboxusercontent.com/u/7995011/IMG_2791.JPG").Result;
                response.EnsureSuccessStatusCode();
                var bytes =  response.Content.ReadAsByteArrayAsync().Result;
                // Save the image to disk

                Debug.WriteLine("Work Done");
            }
        }

        public override bool ShouldRetry(Exception exp)
        {
            return false;
        }
    }
}
