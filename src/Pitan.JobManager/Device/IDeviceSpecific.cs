﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Device
{
   public interface IDevice
    {

        void WakeUp();
        void GoToSleep();
        
    }
}
