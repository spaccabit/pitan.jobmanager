﻿using Pitan.JobManager.Persistance;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Pitan.JobManager
{
    public class JobConsumer
    {

        private static string TAG = typeof(JobConsumer).ToString();
        private JobQueue jobQueue;
        private string name;
        private IPersistentStorage persistanSotrage;
        
        public JobConsumer(string name, JobQueue jobQueue, IPersistentStorage persistanStorage)
        {
            this.name = name;
            this.jobQueue = jobQueue;
            this.persistanSotrage = persistanStorage;
            
        }

        public async void Run()
        {
            while (true)
            {
                Job job = jobQueue.GetNextAvailableJob();

                if (job == null)
                {
                    Task.Delay(3000);
                    continue;
                }

                Debug.WriteLine("Consumer working...");

                JobResult result = await runJob(job);

                if (result == JobResult.DEFERRED)
                {
                    jobQueue.Add(job);
                }
                else
                {
                    if (result == JobResult.FAILURE)
                    {
                        job.Canceled();
                    }

                    if (job.IsPersistan)
                    {
                        persistanSotrage.Remove(job.PersistentId);
                    }

                    if (job.WakeLock && job.WakeLockTimeOut == 0)
                    {
                        job.Sleep();
                    }
                }

                if (job.GroupId != null)
                {
                    jobQueue.IsGroupIdAvaible(job.GroupId);
                }
            }
        }
        
        private async Task<JobResult> runJob(Job job)
        {
            int retryCount = job.RetryCount;
            int runIteration = job.RunIteration;

            while (runIteration <= retryCount)
            {
                try
                {
                    job.Run();
                    return JobResult.SUCCESS;
                }
                catch (Exception exception)
                {
                    if (!job.ShouldRetry(exception))
                    {
                        return JobResult.FAILURE;
                    }
                    else if (!job.IsRequrementsMet)
                    {
                        runIteration++;
                        job.RunIteration = (runIteration + 1);
                        return JobResult.DEFERRED;
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return JobResult.FAILURE;
        }
    }

    public enum JobResult
    {
        SUCCESS,
        FAILURE,
        DEFERRED
    }
}
