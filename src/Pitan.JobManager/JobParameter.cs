using System.Collections.Generic;
using Pitan.JobManager.Requirements;

namespace Pitan.JobManager
{
    public class JobParameter
    {
        public JobParameter(IList<IRequirement> requrements = null, bool isPersistant= false, int retryCount = 0, string groupId = "", bool wakeLock=false, long wakeLockOut = 0)
        {
            this.Requrements = requrements;
            this.IsPersistant = isPersistant;
            this.RetryCount = retryCount;
            this.GroupId = groupId;
            this.WakeLock = wakeLock;
            this.WakeLockOut = wakeLockOut;
        }

        public EncryptionKeys EncryptionKeys { get; set; }

        public string GroupId { get; set; }

        public bool IsPersistant { get; set; }

        public IList<IRequirement> Requrements { get; set; }

        public int RetryCount { get; set; }

        public bool WakeLock { get; set; }

        public long WakeLockOut { get; set; }

        public static JobParameter Builder()
        {
            return new JobParameter();
        }

    }
}