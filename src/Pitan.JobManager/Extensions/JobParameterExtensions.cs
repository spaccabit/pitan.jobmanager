﻿using Pitan.JobManager.Requirements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Extensions
{
    public static class JobParameterExtensions
    {
        public static JobParameter WithRequrements(this JobParameter jobParameter, IRequirement requirement)
        {
            if (jobParameter.Requrements == null)
            {
                jobParameter.Requrements = new List<IRequirement>();
            }

            jobParameter.Requrements.Add(requirement);

            return jobParameter;
        }
        public static JobParameter GroupId(this JobParameter jobParameter, string groupId)
        {
            jobParameter.GroupId = groupId;
            return jobParameter;
        }

        public static JobParameter IsPersistant(this JobParameter jobParameter, bool isPersistant = false)
        {
            jobParameter.IsPersistant = isPersistant;
            return jobParameter;
        }

        public static JobParameter IsWakeLock(this JobParameter jobParameter, bool isWakeLock = false)
        {
            jobParameter.WakeLock = isWakeLock;
            return jobParameter;
        }

        public static JobParameter Retry(this JobParameter jobParameter, int retry = 0)
        {
            jobParameter.RetryCount = retry;
            return jobParameter;
        }

        public static JobParameter WakeLockTimeOut(this JobParameter jobParameter, int timeout = 0)
        {
            jobParameter.WakeLockOut = timeout;
            return jobParameter;
        }
   }
}
