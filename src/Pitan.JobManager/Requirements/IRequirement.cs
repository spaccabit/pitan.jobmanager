﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Requirements
{
    public interface IRequirement
    {
        bool IsPresent();
    }
}
