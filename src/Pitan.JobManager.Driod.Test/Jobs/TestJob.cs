using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Pitan.JobManager.Device;
using System.Threading;

namespace Pitan.JobManager.Driod.Test.Jobs
{
    public class TestJob : Job
    {
        private object ADDED_LOCK = new object();
        private object RAN_LOCK = new object();
        private object CANCELED_LOCK = new object();

        private bool added = false;
        private bool ran = false;
        private bool canceled = false;

        public TestJob(JobParameter jobParameter, IDevice device) : base(jobParameter, device)
        {
           
        }

        public override void Added()
        {
            lock(ADDED_LOCK)
            {
                this.added = true;

                Monitor.PulseAll(ADDED_LOCK);
            }
        }

        public override void Canceled()
        {
           lock(CANCELED_LOCK)
            {
                this.canceled = true;
                Monitor.PulseAll(CANCELED_LOCK);
            }
        }

        public override void Run()
        {
            lock (RAN_LOCK)
            {
                this.ran = true;
                Monitor.PulseAll(RAN_LOCK);
            }
        }

        public bool IsAdded()
        {
            lock (ADDED_LOCK)
            {
                return added;
            }
        }

        public bool IsRan()
        {
            lock (RAN_LOCK)
            {
                return ran;
            }
        }

        public bool IsCanceled()
        {
            lock (CANCELED_LOCK)
            {
                return canceled;
            }
        }

        public override bool ShouldRetry(Exception exp)
        {
            return false;
        }
    }
}