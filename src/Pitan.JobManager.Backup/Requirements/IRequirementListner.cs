﻿namespace Pitan.JobManager.Requirements
{

    public interface IRequirementListener
    {
        void OnRequirementStatusChanged();
    }
}
