﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Device
{
   public interface IDeviceSpecific
    {

        void WakeUp();
        void GoToSleep();
        
    }
}
